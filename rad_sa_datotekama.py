#from PySide2 import QtCore
#import json
import csv
import fileinput
#from itertools import dropwhile, takewhile

#class RadSaDatotekama():
def ucitaj_data_od_do_verzija_1(path, delimiter=",", broj_redova_pocetak=0, broj_redova_kraj=None): # problem je sto dva puta otvaramo istu datoteku u jednoj funkciji, a sama datoteka moze cak biti prevelika da bi se odjednom otvorila.
    """
    Ucitava podatke iz csv-a, deli ih po delimiteru, smesta u listu i vraca tu listu
    :param path: putanja do datoteke
    :param broj_redova_pocetak: od kog reda pocinjemo. Podrazumevano 0 (pocetak)
    :param broj_redova_kraj: do kog reda idemo. Podrazumevano None (ukupan broj redova)
    :param delimiter: delimiter. Podrazumevano "," 
    :return: lista podataka iz datoteke
    """
    try:
        with open(path, "r") as data:     #delimiter=','
            csv_reader = csv.reader(data, delimiter) # mozda staviti da delimiter bude nesto poput: ,%, ili ,
            if broj_redova_kraj == None: # ako je None, moramo ga postaviti da bude jednak broju redova, tj. onda je podrazumevano da idemo do kraja datoteke
                broj_redova_kraj = sum(1 for row in csv_reader)  # brojimo koliko ima redova
                data.seek(0) # resetovanje iteratora # neophodno da bi posle mogli da iterisemo kroz reader i da radimo dajle sa njim
                csv_reader = csv.reader(data, delimiter) # rekreiranje reader objekta # neophodno da bi posle mogli da iterisemo kroz reader i da radimo dajle sa njim
            podaci = []
            for red in csv_reader:
                # proveravamo do kojeg reda smo stigli pomocu countera
                counter = 0                           # ? ili: counter <= broj_redova_kraj - 1:
                if counter >= broj_redova_pocetak and counter <= broj_redova_kraj:
                    podaci.append(red) # imena kolona ce biti na pocetku, kada nam budu trebali podaci bez imena, mozemo ih preskociti sa i + broj_imena. Ili da vratimo touple = (imena_kolona, ostali_podaci)
                    counter +=1
            return podaci
    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        # mozda treba i neki return, npr: return False ili samo return "Datoteka koju pokusavate da otovrite ne postoji"
        # return False

def ucitaj_data_od_do(path, delimiter=",", broj_redova_pocetak=0, broj_redova_kraj=10): # za vece datoteke (sa barem 10 ili vise redova). Sama datoteka opet mora biti ucitana cela, ali ce u listi biti podaci iz po 10 redova
    """
    Ucitava podatke iz csv-a, deli ih po delimiteru, smesta u listu i vraca tu listu
    :param path: putanja do datoteke
    :param broj_redova_pocetak: od kog reda pocinjemo. Podrazumevano 0 (pocetak)
    :param broj_redova_kraj: do kog reda idemo. Podrazumevano 10
    :param delimiter: delimiter. Podrazumevano "," 
    :return: lista podataka iz datoteke
    """
    try:
        with open(path, "r") as data:
            csv_reader = csv.reader(data, delimiter) # mozda staviti da delimiter bude nesto poput: ,%,
            podaci = []
            for red in csv_reader:
                # proveravamo do kojeg reda smo stigli pomocu countera
                counter = 0                           # ? ili: counter <= broj_redova_kraj - 1:
                if counter >= broj_redova_pocetak and counter <= broj_redova_kraj:
                    podaci.append(red) # imena kolona ce biti na pocetku, kada nam budu trebali podaci bez imena, mozemo ih preskociti sa i + broj_imena. Ili da vratimo touple = (imena_kolona, ostali_podaci)
                    counter +=1
            return podaci
    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        return False

def ucitaj_datoteku_parcijalno(path, delimiter=","):
    try:
        podaci = []
        for red in fileinput.input(path, delimiter):
            podaci.append(red)
        return podaci
    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        return False

def ucitaj_podatke(path, path_meta, delimiter=",", limit=10): # od 0 do 10 linija ce izcitati (znaci po 11)
    try:
        with open(path, "r", newline="") as data:
            podaci = []
            i = 0
            #j = 0
            # k sluzi da proveri da li je u pitanju prvi upis ili ne
            k = False # nakon prvog upisa (prvog upisa prve sortirane liste), treba uporedjivati ono sto vec imamo zapisano sa onim sto cemo tek da zapisujemo (uporedjujemo za trenutnom sortiranom listom) 
            for red in data:
                podaci.append(red)
                i+= 1
                if i == limit:  #("pomocna_datoteka" + str(j))
                    with open("pomocna_datoteka", "a") as pomocna: # "a" za append
                        podaci.sort(ucitaj_metadata(path_meta).kljuc) # ili je kljuc u samoj data datoteci, a ne u metadata?
                        najmanji = podaci[0] # treba odraditi proveru najmanjeg iz onoga sto vec imamo u datoteci i onoga sto cemo tek upisati. Ako je to potrebno uraditi swap onog koji se vec nalazi u datoteci i trenutnog najmanjeg 
                        if k: # ovo bi sada trebalo uporedjivati kljuceve (sve one iz vec upisane datoteke sa onima iz liste koje tek treba upisati)
                            #while len(podaci) != 0:
                            for kljuc in pomocna:
                                if pomocna[0] > najmanji:
                                    pomocna[0], najmanji = najmanji, pomocna[0]
                        k = True
                        sacuvaj_data(podaci, pomocna)
                        podaci = []
                        i = 0


            # csv_reader = csv.reader(data, delimiter) 
            # podaci = []
            # for red in csv_reader:
            #         podaci.append(red)
            # return podaci
    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        return False

def ucitaj_data(path, delimiter=","):
    """
    Ucitava podatke iz csv-a, deli ih po delimiteru, smesta u listu i vraca tu listu
    :param path: putanja do datoteke
    :param delimiter: delimiter. Podrazumevano "," 
    :return: lista podataka iz datoteke
    """
    try:
        with open(path, "r", newline="") as data:
            csv_reader = csv.reader(data) # mozda staviti da delimiter bude nesto poput: ,%,
            podaci = []
            for red in csv_reader:
                    podaci.append(red) # imena kolona ce biti na pocetku, kada nam budu trebali podaci bez imena, mozemo ih preskociti sa i + broj_imena (i + broj_kolona, jer imamo po jedno ime (header) za svaku kolonu). Ili da vratimo touple = (imena_kolona, ostali_podaci)
            return podaci
    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        return False
    
def sacuvaj_data(podaci, path, delimiter=","): # pozvati kada se klikne dugme "sacuvaj". Moze se napraviti i da se izabere u podesavanjima aplikacije (koje onda isto treba napraviti) da sacuvava nekon odredjenog broj izmena.
    with open(path, "w", newline="") as za_cuvanje:
        csv_writer = csv.writer(za_cuvanje, delimiter, quotechar='"')
        csv_writer.writerows(podaci)



def izbroj_kolone(path, delimiter=","):
    try:
        with open(path, "r") as data:
            csv_reader = csv.reader(data, delimiter)
            broj_kolona = sum(1 for column in csv_reader[0])
            # da li je data.seek(0) neophodan ovde? Najbole ga ipak ostaviti.
            data.seek(0) # resetovanje iteratora
            # da li je rekreiranje csv_reader-a uopste neophodno ovde? U ovoj funkciji ionako vise necemo nista raditi sa njim.
            csv_reader = csv.reader(data, delimiter) # rekreiranje reader objekta
            return broj_kolona
    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        return False

def izbroj_redove(path, delimiter=","):
    try:
        with open(path, "r") as data:
                csv_reader = csv.reader(data, delimiter)
                broj_redova = sum(1 for row in csv_reader)  # brojimo koliko ima redova
                data.seek(0) # resetovanje iteratora # neophodno da bi posle mogli da iterisemo kroz reader i da radimo dajle sa njim
                csv_reader = csv.reader(data, delimiter)
                return broj_redova
    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        return False

def ucitaj_metadata(path, delimiter=","):
    """
    Ucitava metapodatke iz CSV-a i vraca ih kao recnik, pa da posle mozemo da im pristupimo sa npr. metadata.broj_kolona
    :param path: putanja do datoteke
    :param delimiter: delimiter.
    :return: metadata_dictionary
    """
    try:
        with open(path, "r", newline="") as metadata:
            metadata_dictionary = csv.DictReader(metadata, delimiter=",")
            return metadata_dictionary

    except FileNotFoundError:
        print("Datoteka koju pokusavate da otovrite ne postoji")
        return False