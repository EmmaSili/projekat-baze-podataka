import rad_sa_datotekama
class GenerickaKlasa:
    def __init__(self, path, path_meta):
        self.podaci = rad_sa_datotekama.ucitaj_data(path)
        self.metapodaci = rad_sa_datotekama.ucitaj_data(path_meta)
        self.atributi = self.metapodaci.atributi

        for i in range(self.metapodaci.broj_atributa): # da li podaci da idu od i
            setattr(self, '' + self.atributi[i], self.podaci[i]) # setattr(obj, var, val) znaci da se mora pozvati nad konkretnim objektom
            ...

    #     #for atribut in self.metapodaci.atributi:
    #     for i in range(self.metapodaci.broj_atributa):
    #         #self.atribut = atribut # ovo ce samo svaki put pregaziti self.atribut tako da treba videti kako da se napravi vise atributa (koliko god je potrebno) i u njih smestiti vrednosti promenjive atribut
    #         self.metapodaci.atributi[i] = self.podaci[i]
    #     ...