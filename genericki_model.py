from PySide2 import QtCore
#import csv # ucitavanje podataka premestiti u zaseban modul
#import json
import rad_sa_datotekama


class GenerickiModel(QtCore.QAbstractTableModel):
    # def ucitaj_data(path, delimiter=",", broj_redova_pocetak=0, broj_redova_kraj=None): # mozda staviti delimiter u parametar # mozda staviti i parametar broj_redova, kao broj redova koje ucitavamo odjednom, ili cak da imamo odakle-dokle (broj_redova_pocetak, broj_redova_kraj)
    #     """
    #     Ucitava podatke iz csv-a, deli ih po delimiteru, smesta u listu i vraca tu listu
    #     :param path: putanja do datoteke
    #     :param broj_redova_pocetak: od kog reda pocinjemo. Podrazumevano 0 (pocetak)
    #     :param broj_redova_kraj: do kog reda idemo. Podrazumevano None (ukupan broj redova)
    #     :param delimiter: delimiter. Podrazumevano "," 
    #     :return: lista podataka iz datoteke
    #     """
    #     try:
    #         with open(path, "r") as data:     #delimiter=','
    #             csv_reader = csv.reader(data, delimiter) # mozda staviti da delimiter bude nesto poput: ,%,
    #             if broj_redova_kraj == None: # ako je None, moramo ga postaviti da bude jednak broj redova, tj. onda je podrazumevano da idemo do kraja datoteke
                   
    #                 # !!! mozda razdvojiti i posebno napraviti metodu koja racuna broj redova i posebno metodu koja racuna broj kolona !!!
                   
    #                 broj_redova_kraj = sum(1 for row in csv_reader)  # brojimo koliko ima redova
    #                 data.seek(0) # resetovanje iteratora # neophodno da bi posle mogli da iterisemo kroz reader i da radimo dajle sa njim
    #                 csv_reader = csv.reader(data, delimiter) # rekreiranje reader objekta # neophodno da bi posle mogli da iterisemo kroz reader i da radimo dajle sa njim
    #             podaci = []
    #             for red in csv_reader:
    #                 # proveravamo do kojeg reda smo stigli pomocu countera
    #                 counter = 0                           # ? ili: counter <= broj_redova_kraj - 1:
    #                 if counter >= broj_redova_pocetak and counter <= broj_redova_kraj:
    #                     podaci.append(red) # imena kolona ce biti na pocetku, kada nam budu trebali podaci bez imena, mozemo ih preskociti sa i + broj_imena. Ili da vratimo touple = (imena_kolona, ostali_podaci)
    #                     counter +=1
    #             return podaci
    #     except FileNotFoundError:
    #         print("Datoteka koju pokusavate da otovrite ne postoji")
    #         # mozda treba i neki return, npr: return False ili samo return "Datoteka koju pokusavate da otovrite ne postoji"
    #         # return False
    
    # def izbroj_kolone(path, delimiter=","):
    #     try:
    #         with open(path, "r") as data:
    #             csv_reader = csv.reader(data, delimiter)
    #             broj_kolona = sum(1 for column in csv_reader[0])
    #             # da li je data.seek(0) neophodan ovde? Najbole ga ipak ostaviti.
    #             data.seek(0) # resetovanje iteratora # neophodno da bi posle mogli da iterisemo kroz reader i da radimo dajle sa njim
    #             # da li je rekreiranje csv_reader-a uopste neophodno ovde? U ovoj metodi ionako vise necemo nista raditi sa njim.
    #             csv_reader = csv.reader(data, delimiter) # rekreiranje reader objekta # neophodno da bi posle mogli da iterisemo kroz reader i da radimo dajle sa njim
    #             return broj_kolona
    #     except FileNotFoundError:
    #         print("Datoteka koju pokusavate da otovrite ne postoji")
    #         # mozda treba i neki return, npr: return False ili samo return "Datoteka koju pokusavate da otovrite ne postoji"
    #         # return False
    
    # def izbroj_redove(path, delimiter=","):
    #     # izvuci ono za brojanje redova iz ucitaj_data i staviti ga ovde
    #     ...

    # def ucitaj_metadata(path, delimiter=","):
    #     """
    #     Ucitava metapodatke, mozda kao JSON ili kao CSV i vraca ih, pa da posle mozemo da im pristupimo sa npr. metadata.broj_kolona
    #     :return: metadata_dictionary
    #     """
    #     try:
    #         with open(path, "r") as metadata:
    #             metadata_dictionary = csv.DictReader(metadata, delimiter=",")
    #             return metadata_dictionary

    #     except FileNotFoundError:
    #         print("Datoteka koju pokusavate da otovrite ne postoji")
    #         # mozda treba i neki return, npr: return False ili samo return "Datoteka koju pokusavate da otovrite ne postoji"
    #         # return False
            
                       # da li uopste treba path ovde u init-u?
    def __init__(self, path = "data/visokoskolska_ustanova.csv", path_meta = "data/visokoskolska_ustanovaMETA.csv", delimiter = ",", parent=None, metapodaci = {}, podaci = []): # povratne vrednosti one dve metode gore proslediti ovde u metadata = {}, data = []
        super().__init__(parent)
        self.lista = [] # nije dvodimenzionalni niz
        #self.path = path                                    # path treba da se dobavi kada kliknemo na neki dokument iz stabla dokumenata (structure dock)
        self.metapodaci = rad_sa_datotekama.ucitaj_metadata(path_meta, delimiter) # mozda samo navesti path direktno ovde, bez posredstva promenjive
        if self.metapodaci == False: # ili da stavim da one f-je ne vracaju false i onda ovde da stavim samo if self.metapodaci is not None:
            print("Metapodaci iz datoteke nisu ucitani")
        self.podaci = rad_sa_datotekama.ucitaj_data(path, delimiter) # DODATI PROVERU DA LI JE USPESNO OTVORENA DATOTEKA I STA RADITI AKO NIJE
        if self.podaci == False:
            print("Podaci iz datoteke nisu ucitani")
    # pomocna metoda
    def get_element(self, index):
        return self.lista[index.row()]
    
    #def getAttribute():
    
    # def __getattribute__(self, name: str) -> Any:
    #     return super().__getattribute__(name)

    def rowCount(self, index):
        return len(self.lista)

    def columnCount(self, index):
        return self.metapodaci.broj_kolona
        # ili preko metode za brojanje kolona, ali onda moramo otvoriti datoteku
    
    def data(self, index, role=QtCore.Qt.DisplayRole):
        # TODO: dodati obradu uloga (role)
        informacioni_resurs = self.get_element(index)
        for i in range(self.metapodaci.broj_atributa): # da li je broj kolona jednak broju atributa? I onda je sve to jednako broju header-a?
            if index.column() == i and role == QtCore.Qt.DisplayRole:
                #return informacioni_resurs.getAttribute(self.metapodaci.kolone[i]) # ili metapodaci[i] ili kolone_atributa[i] ili atributi[i], dakle poenta je da moramo imati listu u kojoj su svi atributi informacionog_resursa 
                #return informacioni_resurs.getAttribute(self.podaci[i + self.metapodaci.broj_kolona])

                #return informacioni_resurs.__getattribute__(self.podaci[i + self.metapodaci.broj_kolona])
                return informacioni_resurs.__getattribute__(self.podaci[i])
            #return None

        # if index.column() == 0 and role == QtCore.Qt.DisplayRole:
        #     return informacioni_resurs.broj_indeksa
        # elif index.column() == 1 and role == QtCore.Qt.DisplayRole:
        #     return informacioni_resurs.ime_prezime
        # return None

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        for i in range(self.metapodaci.broj_kolona): # broj_kolona = broj_headera
            if section == i and orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
                return self.podaci[i]
                #return  self.metapodaci.headeri[i] # return  self.metapodaci.kolone[i]
            #return None

        # if section == 0 and orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
        #     return "Broj indeksa"
        # elif section == 1 and orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
        #     return "Ime i prezime"
        # return None

    # metode za editable model
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        informacioni_resurs = self.get_element(index)
        for i in range(self.metapodaci.broj_atributa): # da li je broj kolona jednak broju atributa? I onda je sve to jednako broju header-a?
            if value == "":
                return False
            if index.column() == i and role == QtCore.Qt.EditRole:
                #informacioni_resurs.getAttribute(self.podaci[i + self.metapodaci.broj_kolona]) = value
                #informacioni_resurs.__getattribute__(self.podaci[i + self.metapodaci.broj_kolona]) = value
                
                #informacioni_resurs.__getattribute__(self.podaci[i]) = value
                informacioni_resurs[self.podaci[i]] = value
                rad_sa_datotekama.sacuvaj_data(informacioni_resurs)

                # TODO: cuvanje se moze raditi ovde, posle svake izmene, ali to nije efikasno. Napraviti dugme "Sacuvaj izmene" i povezati cuvanje sa njim

                #informacioni_resurs.getAttribute(self.metapodaci.kolone[i]) = value
                return True
            return False

        # if value == "":
        #     return False
        # if index.column() == 0 and role == QtCore.Qt.EditRole:
        #     informacioni_resurs.broj_indeksa = value
        #     return True
        # elif index.column() == 1 and role == QtCore.Qt.EditRole:
        #     informacioni_resurs.ime_prezime = value
        #     return True
        # return False

    def flags(self, index):
        return super().flags(index) | QtCore.Qt.ItemIsEditable # ili nad bitovima